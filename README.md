# 这是一个中期存档使用的仓库

项目名称：Linux Lab openEuler 集成开发支持
项目编号：210560288

# qemu aarch64/virt board support packages or BSPs

This is for [Linux Lab](https://tinylab.org/linux-lab).

## Boot it

    $ ./boot.sh

## Use it in Linux Lab

    $ cd /path/to/linux-lab
    $ make B=aarch64/virt
    $ make boot

# linux-lab macro support for switching default kernel and openEuler kernel from command line

Check [this repo](https://gitee.com/riumin/linux-lab-oel/tree/gitee-publish/) for details.